FROM openjdk:12
ADD target/docker-spring-boot-mauricio_schwelm.jar docker-spring-boot-mauricio_schwelm.jar
EXPOSE 8085
ENTRYPOINT ["java", "-jar", "docker-spring-boot-mauricio_schwelm.jar"]
